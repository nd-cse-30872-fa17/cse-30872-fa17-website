#!/usr/bin/env python2.7

import collections
import csv
import glob
import os
import yaml

# Constants

GRADES   = 'data/FA17_CSE_30872_01.csv'
TAS      = 'static/yaml/ta.yaml'
MAPPINGS = 'static/yaml/reading*.yaml'

# Grades

def load_grades(path):
    grades = {}
    for student in csv.DictReader(open(path)):
        data = {}
        for k, v in student.items():
            k = ''.join(k.lower().split()).split('(')[0]
            data[k] = v
        grades[data['studentid']] = data

    return grades

# Mappings

def load_mappings(pattern):
    mappings = {}
    for path in glob.glob(pattern):
        name  = os.path.basename(path).split('.')[0]
        index = int(name[-2:])
        data  = collections.defaultdict(list)
        for student, ta in yaml.load(open(path)):
            data[ta].append(student)
        
        mappings[name] = data
        mappings['challenge{:02d}'.format(index * 2)] = data
        if index > 0:
            mappings['challenge{:02d}'.format(index * 2 - 1)] = data

    return mappings

def find_missing(mappings, grades):
    missing = collections.defaultdict(dict)

    for assignment, mapping in mappings.iteritems():
        for ta, students in mapping.iteritems():
            missing[ta][assignment] = []
            for student in students:
                if not grades[student][assignment]:
                    missing[ta][assignment].append(student)

    return missing

# Main Execution

if __name__ == '__main__':
    grades   = load_grades(GRADES)
    mappings = load_mappings(MAPPINGS)
    missing  = find_missing(mappings, grades)

    for ta, assignments in sorted(missing.iteritems()):
        print ta
        for assignment, students in sorted(assignments.iteritems()):
            print '{:>15}: {}'.format(assignment, ', '.join(students))
        print

