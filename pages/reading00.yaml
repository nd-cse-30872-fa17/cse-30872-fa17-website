title:      "Reading 00: I/O"
icon:       fa-book
navigation: []
internal:
external:
body:       |

    **Everyone**:

    Welcome to [CSE 30872 Programming Challenges], which (as the syllabus
    states) is a "course that revolves around solving *brain-teaser* and
    *puzzle-type* problems that often appear in programming contests, online
    challenges, and job interviews".  What this means is that we will be
    studying common data structures, algorithms, and programming techniques
    that are useful in tackling a variety of problems.

    <div class="alert alert-info" markdown="1">

    #### <i class="fa fa-bookmark"></i> TL;DR

    For this week, you need to setup your [GitLab] repository, read about I/O
    and complexity, and submit your responses to the [Reading 00 Quiz].

    </div>

    ## Course Overview

    Last semester, many of you took the data structures course where you
    learned about the properties and characteristics of different data
    structures (and perhaps you even implemented a few).  The focus of this
    class, however, is not in the *construction* of these data structures, but
    rather their *application*.  This means we will focus on *when* to use
    these data structures and algorithms and *how* to utilize them effectively
    rather than *what* they are.  In concrete terms, we will not be
    implementing say a hash table; instead we will use it to solve a variety of
    problems.

    Following the [Hands-On Imperative], you will have the opportunity to put
    the material we discuss in class into practice via numerous weekly
    **programming challenges** (about 2 a week).  Along with these challenges
    will be **reading assignments** to ensure you have a context or background
    knowledge for what we will be discussing in class.  Unlike last semester,
    the reading assignments will be automatically graded as explained below.

    In fact all of your assignments will be automatically graded by our
    [continuous integration] system.  As you may have experienced in your
    internship this summer, **testing** is an important part of software
    engineering and we will briefly explore various aspects of testing
    throughout the semester along with other software engineering techniques
    such as pair programming.

    Although each class will involve some amount of lecture, there will also be
    a lot of in-class activities and hands-on learning.  Because of this,
    participation is a component of your grade and you are expected to come to
    class regularly and on-time.

    Fortunately, however, there are no exams in this class.  Instead, we will
    have 2 in-class programming contests where you will get to work in groups
    of 3 (similar to the [ACM Programming Contest]).  Likewise, you will also
    have to participate in an *external* programming contest such as the
    regional [ACM Programming Contest] or any other competitive programming
    event (online is allowed).

    [Hands-On Imperative]:      https://en.wikipedia.org/wiki/Hacker_ethic#Hands-On_Imperative
    [Continuous Integration]:   https://en.wikipedia.org/wiki/Continuous_integration
    [ACM Programming Contest]:  https://icpc.baylor.edu/

    ## Task 1: GitLab

    For this week, your first task is to setup your [GitLab] repository by doing the following:

    1. **Fork** the class assignments repository:
      [CSE-30872-FA17-Assignments](https://gitlab.com/nd-cse-30872-fa17/cse-30872-fa17-assignments)

        <div class="thumbnail">
        <img src="static/img/gitlab-fork.png" class="img-responsive"/>
        </div>

    2. **Configure** the **Settings** of your repository such that:

        - Under **General project settings** > **Project description (optional)**, record your name and netid.

            <div class="thumbnail">
            <img src="static/img/gitlab-project-description.png" class="img-responsive"/>
            </div>

        - Under **Sharing and permissions** > **Project visibility**, select **Private**.

            <div class="thumbnail">
            <img src="static/img/gitlab-project-visibility.png" class="img-responsive"/>
            </div>

    3. **Add** the instructional staff to your repository:

        - Under **Project members**, select the ***Add member** tab.

        - Under **Project members** > **Select member to invite**, enter in
          the following accounts:

            1. Peter Bui (**@pbui**)
            2. Catherine Badart (**@cbadart**)
            3. Nikolas Brooks (**@nbrooks3**)
            4. Thomas Flanagan (**@Quarke**)
            5. Patricia "Happy" Hale (**@phale1**)
            6. Zach Janicki (**@zachjanicki**)
            7. John Johnson (**@jrjohnson1231**)
            8. Owen Phelan (**@ophelan**)

        - Under **Project members** > **Choose a role permission**, select
          **Developer**.

        - Under **Project members** > **Access expiration date**, select
          **January 1, 2018** (`2018-01-01`).


        <div class="thumbnail">
        <img src="static/img/gitlab-project-members.png" class="img-responsive"/>
        </div>

    4. Once this is done, you can [clone] your [git] repository to your local machine:

            :::bash
            $ git clone https://gitlab.com/$USERNAME/cse-30872-fa17-assignments.git

        **Note**: If you have not done so already, you should consider setting
        up [SSH Keys](https://docs.gitlab.com/ce/ssh/README.html) on [GitLab]
        to enable password-less pushing and pulling.


    [clone]:    https://git-scm.com/docs/git-clone
    [git]:      https://git-scm.com/

    <div class="alert alert-danger" markdown="1">
    #### <i class="fa fa-gitlab"></i> GitLab Repository

    You are responsible for ensuring that your [GitLab] assignments repository
    is in proper order, which means you have the correct settings and
    permissions set.  Failure to properly configure your repository will
    adversely impact your grade.

    </div>

    ## Task 2: Reading

    The readings for **Wednesday, August 23** are

    1. [Competitive Programmer's Handbook]

        - Chapter 1 Introduction

        - Chapter 2 Time Complexity

    2. [Git Branching](https://git-scm.com/docs/git-branch)

        *Consider doing the [Learning Git
        Branching](http://learngitbranching.js.org/) tutorial if you are not
        familiar with branching.*

    **Note**: Don't worry if you don't get the readings done by Wednesday, as
    this is the first week of class.  Just do what you can (be sure to checkout
    the eclipse though :).

    <div class="alert alert-danger" markdown="1">
    #### <i class="fa fa-sun-o"></i> Partial Eclipse

    While enjoying the eclipse, be sure to **not** look directly at the sun
    unless you have protective glasses (normal sunglasses are not sufficient).

    </div>

    ## Task 3: Quiz

    Once you have done the readings, answer the following [Reading 00 Quiz]
    questions:

    <div id="quiz-questions"></div>

    <div id="quiz-responses"></div>

    <script src="static/js/dredd-quiz.js"></script>
    <script>
    loadQuiz('static/json/reading00.json');
    </script>

    To submit your answers, you will need create a `answers.json` or
    `answers.yaml` file in the `reading00` folder of your **assignments**
    repository:

    1. For this class, you must use a **separate [git] branch for each
    assignment**.  This means that the work for each reading and challenge must
    be done in a separate branch.  To create and checkout a new branch, you can
    do the following:

            :::bash
            $ git checkout master         # Make sure we are in master branch
            $ git pull --rebase           # Make sure we are up-to-date with GitLab repository

            $ git checkout -b reading00   # Create reading00 branch and check it out

        Once you do the above, you should see the following output for the
        [git-branch] command:

            :::bash
            $ git branch
              master
            * reading00

        The `*` indicates that we are currently on the `reading00` branch.

    2. You can either hand-write the `answers` file using your favorite text
    editor or you can use the online form to generate the [JSON] data.

        A hand-written `answers.yaml` may look like the following:

            :::text
            q1:     [python,perl,ruby]
            q2:     [n2,nf,nlogn,1,n,sqrtn,logn]
            q3:     n
            q4:     n

        The equivalent `answers.json` generated using the online form may look
        like the following:

            :::json
            {
              "q1": [
                "python",
                "perl",
                "ruby"
              ],
              "q2": [
                "n2",
                "nf",
                "nlogn",
                "1",
                "n",
                "sqrtn",
                "logn"
              ],
              "q3": "n",
              "q4": "n"
            }

        You may use either format.  To determine which symbols correspond to
        which response, take a look at the [Reading 00 Quiz] file.

        To check your answers, you can use the provided `.scripts/submit.py`
        script:

            :::bash
            $ cd reading00                        # Go into reading00 folder
            $ $EDITOR answers.json                # Edit your answers.json file

            $ ../.scripts/submit.py               # Submit reading00
            Submitting reading00 assignment ...
            Submitting reading00 quiz ...
                  Q1 0.14
                  Q2 0.14
                  Q3 0.00
                  Q4 0.00
               Score 0.29

        This script will submit your `reading00/answers.json` file to [dredd],
        which is the automated grading system.  [dredd] will take your answers
        and return to you a score as shown above.  Each reading is worth
        **2.0** points.

        **Note**: You may submit your quiz answers as many times as you want;
        [dredd] does not keep track of who submits what or how many times.  It
        simply returns a score.

    3. Once you have your answers file, you need to add, commit the file, and
    push your commits to [GitLab]:

            :::bash
            $ git add answers.json                # Add answers.json to staging area
            $ git commit -m "Reading 00: Done"    # Commit work

            $ git push -u origin reading00        # Push branch to GitLab

        **Note**: You may edit and commit changes to your branch as many times
        as you wish.  Just make sure all of your work goes in the appropriate
        branch and then perform a `git push` when you are done.

    4. When you are ready for your final submission, you need to [create a merge request]
        via the [GitLab] interface:


        - First, go to your repository's **Branches** page and then press the
        **Merge Request** button for the appropriate branch:

            <div class="thumbnail">
            <img src="static/img/gitlab-branches.png" class="img-responsive"/>
            </div>

        - Next, click on the "Change branches" link so you can set the **Target
          branch** to your own repository (rather than the class repository).
            
            <div class="thumbnail">
            <img src="static/img/gitlab-new-merge-request.png" class="img-responsive"/>
            </div>

            Failure to select your own repository will lead to you
            unintentionally sharing your answers to the class, so please make
            sure you select your repository and not the class one.

        - Finally, assign the merge request to the appropriate TA (reference
          the [Reading 00 TA List]) and submit the merge request.

            <div class="thumbnail">
            <img src="static/img/gitlab-merge-request.png" class="img-responsive"/>
            </div>

        Every commit on [GitLab] will automatically submit your quiz or code to
        [dredd] and the results of each run is displayed in the merge request
        as a **Pipeline** task.  To view the results of the submission, click
        on the **Pipeline** number and then the **test** button.  You should
        then be able to see the results of the automated test, which should
        look something like this:

        <div class="thumbnail">
        <img src="static/img/gitlab-pipeline.png" class="img-responsive"/>
        </div>

    Once you have made the merge request, the TA can verify your work and
    provide feedback via the discussion form inside the merge request.  If
    necessary, you can update your submission by simply commit and pushing to
    the appropriate branch; the merge request will automatically be updated to
    match your latest work.

    When all work is complete, the TA will merge your branch and close the
    merge request.

    <div class="alert alert-success" markdown="1">

    #### <i class="fa fa-thumbs-up"></i> Qualitative Feedback

    The purpose of this workflow is to provide you with better feedback.
    Instead of having TAs be in charge of simply determining if your work is
    correct, they will be responsible for providing you with more **qualitative
    feedback** such as whether or not you used good programming practices or if
    your code could be improved or organized better.

    The hope is that this will help you grow as programmers and develop some
    **taste** :).

    </div>

    [CSE 30872 Programming Challenges]:     index.html
    [Competitive Programmer's Handbook]:    https://cses.fi/book.html
    [GitLab]:                               https://gitlab.com
    [Reading 00 Quiz]:                      static/json/reading00.json
    [JSON]:                                 http://www.json.org/
    [git-branch]:                           https://git-scm.com/docs/git-branch
    [dredd]:                                https://dredd.h4x0r.space
    [create a merge request]:               https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html
    [Reading 00 TA List]:                   reading00_tas.html
